<?php

class ArticulosController extends Controller
{
	public $_model;
	public function actionIndex()
	{
		$this->render('index');
	}

public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}
public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Articulos::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
 public function actionCreate()
    {
        $model=new Articulos;
        $path_picture = realpath( Yii::app( )->getBasePath( )."/../images/articulos" )."/";//ruta final de la imagen
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Articulos']))
        {   
//if ($model->validate()) {

            $model->attributes=$_POST['Articulos'];
            $fileName = "test"; 
             $fileName2 = "testxx"; 
            $uploadedFile=CUploadedFile::getInstance($model,'foto1');
             $uploadedFile2=CUploadedFile::getInstance($model,'foto2');
            if(!empty($uploadedFile))  // check if uploaded file is set or not
            {   //$uploadedFile->saveAs(Yii::app()->basePath.'/../banner/'.$fileName);  // image will uplode to rootDirectory/banner/
                $uploadedFile->saveAs($path_picture.$fileName);
                $model->foto1= $fileName;
            }
            if(!empty($uploadedFile2))  // check if uploaded file is set or not
            { echo "hay uan imagen";
                //$uploadedFile->saveAs(Yii::app()->basePath.'/../banner/'.$fileName);  // image will uplode to rootDirectory/banner/
                $uploadedFile2->saveAs($path_picture.$fileName2);
                $model->foto2= $fileName;
            }
            if($model->save())  { $this->redirect(array('view','id'=>$model->id));}
  //          }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

	public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    } 



 public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
}