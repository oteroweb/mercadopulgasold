<?php

class CategoriasController extends Controller
{
	public $_model; public $layout='//layouts/column2';
	public function actionIndex()
	{
				$dataProvider=new CActiveDataProvider('Categorias');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


  public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    } 



 public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }




  public function actionCreate()
    {
        $model=new Categorias;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Categorias']))
        {
            $model->attributes=$_POST['Categorias'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }






  public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Categorias']))
        {
            $model->attributes=$_POST['Categorias'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }



    public function actionAdmin()
    {
        $model=new Categorias('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Categorias']))
            $model->attributes=$_GET['Categorias'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }




protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='categorias-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }








public function actionView()
	{

          $criteria=new CDbCriteria();
          $criteria->params = array(':id' => $_GET['id']);
// 
    $count=Articulos::model()->count($criteria);
    $pages=new CPagination($count);

    // results per page
    $pages->pageSize=6;
    $pages->applyLimit($criteria);
    $models=Articulos::model()->findAll($criteria);


		$this->render('view',array(
			'model'=>$this->loadModel(),
            'models' => $models,
            'pages'=> $pages,
		));
	}

		public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Categorias::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}