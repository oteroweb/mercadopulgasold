<?php
$this->breadcrumbs=array(
	Shop::t('Products'),
);
Shop::renderFlash();
?>

<h2><?php echo Shop::t('All products'); ?></h2>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 

$this->Widget('CTreeView', array(
	'data' => Category::getChilds(0),
	'animated' => 'slow',	
	'collapsed' => 'false',	
	'persist' => 'cookie',	
	));




?>
