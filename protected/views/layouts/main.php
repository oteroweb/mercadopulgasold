<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="es">
    
    <!-- agregar jsquery -->
    <script src="js/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="/mercadopulgas/css/estilos.css">

    <?php 
     // Yii::app()->getClientScript()->registerCoreScript('jquery');
      // Yii::app()->getClientScript()->registerCoreScript('bootstrap');

     // Yii::app()->bootstrap->registerCoreCss(); 

     ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="navbar navbar-default ">
  <!-- <div class="navbar navbar-default navbar-fixed-top"> -->

       
             <?php 



             $this->widget('booster.widgets.TbNavbar', array(
                        'type' => 'MP',
                        'brand' =>  CHtml::image(Yii::app()->request->baseUrl.'/images/logo.svg', 'Mercadopulgas', array('title'=>'image title here')),
                        'brandUrl' => Yii::app()->request->baseUrl,
                        'collapse' => true, // requires bootstrap-responsive.css
                        'fixed' => false,
                        'fluid' => false,
                        'items' => array(
                                         '<form class="navbar-form navbar-left" action=""><div class="form-group"><input type="text" class="form-control" placeholder="Search"></div><button style="font-size: 21px; padding: 0px 5px;" class="btn btn-info btn-sm" type="submit"><i class="glyphicon glyphicon-globe"></i></button></form>',

                                    array('class' => 'booster.widgets.TbMenu',
                                           'type' => 'navbar',
                                           'htmlOptions' => array('class' => 'pull-right'),
                                'items' => array
                                            (array('url'=>Yii::app()->getModule('user')->registrationUrl, 'label'=>Yii::app()->getModule('user')->t("Register"), 'visible'=>Yii::app()->user->isGuest),

                                            array('url'=>Yii::app()->getModule('user')->loginUrl, 'label'=>Yii::app()->getModule('user')->t("Login"), 'visible'=>Yii::app()->user->isGuest),
                                    

                                     array('label' => 'Mi cuenta','url' => '#',
                                        'items' => array(
                                            array('label' => 'Mis Ventas', 'url' => '#'),
                                            array(
                                                'label' => 'Mis Compras',
                                                'url' => '#'
                                            ),
                                            '---',
                                            array('url'=>Yii::app()->getModule('user')->profileUrl, 'label'=>Yii::app()->getModule('user')->t("Profile"), 'visible'=>!Yii::app()->user->isGuest),
                                            array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->getModule('user')->t("Logout").' ('.Yii::app()->user->name.')', 'visible'=>!Yii::app()->user->isGuest),

                                            array('url'=>array('/articulos/create'), 'label'=>Yii::app()->getModule('user')->t("Vender"), 'visible'=>!Yii::app()->user->isGuest),
                                        ),'visible'=>!Yii::app()->user->isGuest, 
                                    ),
                                            ),

                            ),
                        ),
                    ));


                    ?>
       

    </div>
  	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
<div class="container-fluid">
    
    <?php echo $content; ?>
</div>


<!-- page -->

</body>


</html>
 