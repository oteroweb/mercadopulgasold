<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="es">
    
    <!-- agregar jsquery -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/mercadopulgas/css/estilos.css">

    <?php 
     // Yii::app()->getClientScript()->registerCoreScript('jquery');
      // Yii::app()->getClientScript()->registerCoreScript('bootstrap');

     // Yii::app()->bootstrap->registerCoreCss(); 

     ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="navbar navbar-default ">
  <!-- <div class="navbar navbar-default navbar-fixed-top"> -->

       
             <?php 



             $this->widget('booster.widgets.TbNavbar', array(
                        'type' => 'MP',
                        'brand' =>  CHtml::image(Yii::app()->request->baseUrl.'/images/logo.svg', 'Mercadopulgas', array('title'=>'image title here')),
                        'brandUrl' => array('site/index'),
                        'collapse' => true, // requires bootstrap-responsive.css
                        'fixed' => false,
                        'fluid' => false,
                        'items' => array(
                                         '<form class="navbar-form navbar-left" action=""><div class="form-group"><input type="text" class="form-control" placeholder="Search"></div><button style="font-size: 21px; padding: 0px 5px;" class="btn btn-info btn-sm" type="submit"><i class="glyphicon glyphicon-globe"></i></button></form>',

                                    array('class' => 'booster.widgets.TbMenu',
                                           'type' => 'navbar',
                                           'htmlOptions' => array('class' => 'pull-right'),
                                'items' => array
                                            (array('label' => 'Registrate', 'url' => '#', 'itemOptions' => array( 'data-toggle' => 'modal', 'data-target' => '#registro',)
                                                  ),

                                            array('label' => 'Login', 'url' => '#', 'itemOptions' => array( 'data-toggle' => 'modal','data-target' => '#login',)),
                                    

                                     array('label' => 'Mi cuenta','url' => '#',
                                        'items' => array(
                                            array('label' => 'Mis Ventas', 'url' => '#'),
                                            array(
                                                'label' => 'Mis Compras',
                                                'url' => '#'
                                            ),
                                            '---',
                                            array('label' => 'Especiales', 'url' => '#'),
                                            array('label' => 'Cerrar Sesion', 'url' => array('site/logout'),'visible'=>!Yii::app()->user->isGuest),

                                        ),'visible'=>!Yii::app()->user->isGuest, 
                                    ),
                                                    array('label'=>'Login', 'url'=>array('site/login'), 'visible'=>!Yii::app()->user->isGuest),
                                            ),

                            ),
                        ),
                    ));


                    ?>
       

    </div>
    <?php $this->beginWidget('booster.widgets.TbModal',array('id' => 'login')); ?> 

    <div class="modal-header"><a class="close" data-dismiss="modal">&times;</a><h4>Iniciar Seccion</h4></div>
     
    <div class="modal-body">
        <?php  $login= new LoginForm; ?>
            <div class="form">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'login-form',
                    'action'=>$this->createUrl("site/login"),
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); 
                ?>
            <p class="note">Fields with <span class="required">*</span> are required.</p>
            <div class="row">
                <?php echo $form->labelEx($login,'username'); ?>
                <?php echo $form->textField($login,'username'); ?>
                <?php echo $form->error($login,'username'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($login,'password'); ?>
                <?php echo $form->passwordField($login,'password'); ?>
                <?php echo $form->error($login,'password'); ?>
                <p class="hint">
                    Hint: You may login with <kbd>demo</kbd>/<kbd>demo</kbd> or <kbd>admin</kbd>/<kbd>admin</kbd>.
                </p>
            </div>

            <div class="row rememberMe">
                <?php echo $form->checkBox($login,'rememberMe'); ?>
                <?php echo $form->label($login,'rememberMe'); ?>
                <?php echo $form->error($login,'rememberMe'); ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton('Login'); ?>
            </div>
            </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="modal-footer">Mercadopulgas</div>
    <?php $this->endWidget(); 
       $this->beginWidget('booster.widgets.TbModal',
            array('id' => 'registro')
        ); $form = $this->beginWidget('booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );
    $XX= new Usuarios;
    echo $form->textFieldGroup($XX, 'email');
    echo $form->passwordFieldGroup($XX, 'pass');
    echo $form->textFieldGroup($XX, 'email');
    echo $form->passwordFieldGroup($XX, 'pass');
    echo $form->textFieldGroup($XX, 'email');
    echo $form->passwordFieldGroup($XX, 'pass');
    echo $form->textFieldGroup($XX, 'email');
    echo $form->passwordFieldGroup($XX, 'pass');
    echo $form->textFieldGroup($XX, 'email');
    echo $form->passwordFieldGroup($XX, 'pass');
    echo $form->textFieldGroup($XX, 'email');
    echo $form->passwordFieldGroup($XX, 'pass');

    $this->widget('booster.widgets.TbButton',array('buttonType' => 'submit', 'label' => 'Login'));
    $this->endWidget();
    unset($form);
        $this->widget('booster.widgets.TbButton',
                        array(
                            'label' => 'Close',
                            'url' => '#',
                            'htmlOptions' => array('data-dismiss' => 'modal'),
                        )
                    ); 
    $this->endWidget(); ?>

    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif?>
<div class="container-fluid">
    
    <?php echo $content; ?>
</div>


<!-- page -->

</body>


</html>
 