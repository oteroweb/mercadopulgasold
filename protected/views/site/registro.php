<div class="form">
	<?php 
$form= $this->beginWidget('CActiveForm', array (
	'method'=>'POST',
	'action'=>Yii::app()->createUrl('site/registro'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'validateOnChange'=>true,
		'validateOnType'=>true,

		),
	));
	 ?>

	<div class="row">
		
<?php 
echo $form->labelEx($model,'nombre'); 
echo $form->TextField($model,'nombre');
echo $form->error($model,'nombre');
?>
	</div> 


<div class="row">
	<?php 
	echo $form->labelEx($model,'email'); 
	echo $form->TextField($model,'email');
	echo $form->error($model,'email');
	?>
</div> 
<div class="row">
	<?php 
	echo $form->labelEx($model,'password'); 
	echo $form->passwordField($model,'password');
	echo $form->error($model,'password');
	?>
</div> 
<div class="row">
	<?php 
	echo $form->labelEx($model,'repetir'); 
	echo $form->passwordField($model,'repetir');
	echo $form->error($model,'repetir');
?>
</div> 

<div class="row">
	<?php 
	echo $form->labelEx($model,'termino'); 
	echo $form->passwordField($model,'termino');
	echo $form->error($model,'termino');
?>
</div> 

	<?php if(extension_loaded('gd')): ?>
  <div class="row">
    <?php echo $form->labelEx($model,'verifyCode'); ?>
    <div>
      <?php $this->widget('CCaptcha'); ?>
      <?php echo $form->textField($model,'verifyCode'); ?>
    </div>
    <div class="hint">Introduzca las letras que aparecen arriba.
    <br/>No hay distinción entre mayúsculas y minúsculas.</div>
    <?php echo $form->error($model,'verifyCode'); ?>
  </div>
<?php endif; ?>









<div class="row">
	<?php echo CHtml::submitbutton('Registrarme'); ?>
</div> 
<?php $this->endWidget(); ?>

</div>

