<?php
/* @var $this ArticulosController */
/* @var $model Articulos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'articulos-form',
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	//'enableAjaxValidation'=>true,
	 'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'validateOnChange'=>true,
		'validateOnType'=>true,

		),
     

)); ?>

<?php echo $form->error($model, 'foto1');  ?>
	<div class="row">
	<?php echo $form->labelEx($model, 'foto1');
echo $form->fileField($model, 'foto1', array ('accept' => ".jpg,.jpeg,.png,.svg"));
echo $form->error($model, 'foto1'); ?>

<?php echo $form->labelEx($model, 'foto2');
echo $form->fileField($model, 'foto2', array ('accept' => ".jpg,.jpeg,.png,.svg"));
echo $form->error($model, 'foto2'); ?>


		<?php echo $form->labelEx($model,'id_cat'); ?>
		<?php echo CHtml::dropDownList('Articulos[id_cat]', $model, 
              CHtml::listData(Categorias::model()->findAll(),
              'id', 'categoria'), array('empty' => '(Seleccionar categoria)'));

		 ?>
		<?php echo $form->error($model,'id_cat'); ?>
	</div>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'articulo'); ?>
		<?php echo $form->textField($model,'articulo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'articulo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subtitulo'); ?>
		<?php echo $form->textField($model,'subtitulo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'subtitulo'); ?>
	</div>

	<div class="row">
		<?php 
		// echo $form->labelEx($model,'foto1'); ?>
		<?php // echo $form->textField($model,'foto1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'foto1'); ?>
	</div>

	<div class="row">
		<?php 
		// echo $form->labelEx($model,'foto2'); ?>
		<?php 
		// echo $form->textField($model,'foto2',array('size'=>60,'maxlength'=>255)); ?>
		<?php
		 // echo $form->error($model,'foto2'); ?>

	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->dropDownList($model,'estado', array('nuevo' => 'Nuevo', 'usado' => 'Usado')); ?>

		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stock'); ?>
		<?php echo $form->textField($model,'stock'); ?>
		<?php echo $form->error($model,'stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'precio'); ?>
		<?php echo $form->numberField($model,'precio').'Bs.F.'; ?>
		<?php echo $form->error($model,'precio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipopago'); ?>
		<?php echo $form->textField($model,'tipopago',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tipopago'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php if (isset($_POST['Articulos']['descripcion'])) { $test= html_entity_decode($_POST['Articulos']['descripcion']); } else $test = "";
        $this->widget(
        'booster.widgets.TbCKEditor',
        array(
        'model' => $model,
        'attribute'=> 'descripcion',
        // 'htmlOptions' => array('value'=> $model->descripcion),
        	'htmlOptions' => array('value'=> $test),
        'value' => "null",
            // 'name' => 'Articulos[descripcion]',
        // CKEDITOR.instances['Articulos_descripcion'].getData()
            'editorOptions' => array(
            	// 'fullpage'=>'js:true', 
                // From basic `build-config.js` minus 'undo', 'clipboard' and 'about'
                // 'plugins' => 'basicstyles,toolbar,enterkey,entities,floatingspace,wysiwygarea,indentlist,link,list,dialog,dialogui,button,indent,fakeobjects'
            // 'toolbar' => "js:[                //  format #3:  javascript code
            //  ['Source', '-', 'Save']
            // ]",


            ),
            // 'string'=>null,
        )
    );


    // $this->widget(
    //     'booster.widgets.TbRedactorJs',
    //     [
    //         'name' => 'Articulos[descripcion]',
    //         'value' => '<b>Here is the text which will be put into editor view upon opening.</b>',
    //     ]
    // );
          

		?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'garantia'); ?>
		<?php echo $form->dropDownList($model,'garantia', array('1' => 'Si', '0' => 'No')); ?>
		<?php echo $form->error($model,'garantia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipopublicacion'); ?>
		<?php echo $form->dropDownList($model,'garantia', array('basico' => 'Basico', 'dorado' => 'Dorado')); ?>
		<?php echo $form->error($model,'tipopublicacion'); ?>
	</div>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->