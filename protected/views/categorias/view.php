<?php
// $this->breadcrumbs=array(
// 	Yii::t('ShopModule.shop', 'Shop')=>array('shop/index'),
// 	Yii::t('ShopModule.shop', 'Categories')=>array('index'),
// 	$model->categoria,
// );

?>

<h2> <?php  echo $model->categoria; ?></h2>
<div class="row">
    <div class="col-sm-3 col-md-3 sidebar"> 
        <?php 
            $this->renderPartial('/layouts/categoria');
            $this->pageTitle="home";
        ?>  


    </div>

     <div class="col-sm-9 col-md-9 "> 
     		<div class="row">
				<?php foreach($model->Articulos as $product) 
				{ 
				$this->renderPartial('/articulos/_view', array('data' => $product,   'pages'=> $pages,));
				}
				?>
  			</div>
     <div class="row">
            <div class="col-md-12">
            <?php 
            $this->widget('CLinkPager', array( 'pages' => $pages, )) 
            ?>
            </div>
      </div>
    </div>
     
    
</div>

<!-- <div class="clear"> </div> -->


