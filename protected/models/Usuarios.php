<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property integer $id
 * @property string $email
 * @property string $fname
 * @property string $lname
 * @property string $direccion
 * @property string $pass
 * @property string $date
 * @property string $expdate
 * @property string $status
 * @property string $user_type
 * @property string $usuario
 * @property string $telefono
 * @property integer $documento
 * @property integer $id_ciu
 * @property double $balance
 *
 * The followings are the available model relations:
 * @property Articulos[] $articuloses
 */
class Usuarios extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, fname, lname, direccion, pass, date, expdate, status, user_type, usuario, telefono, documento, id_ciu, balance', 'required'),
			array('documento, id_ciu', 'numerical', 'integerOnly'=>true),
			array('balance', 'numerical'),
			array('email, fname, lname, direccion, pass, usuario', 'length', 'max'=>255),
			array('status, user_type', 'length', 'max'=>1),
			array('telefono', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, fname, lname, direccion, pass, date, expdate, status, user_type, usuario, telefono, documento, id_ciu, balance', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articuloses' => array(self::HAS_MANY, 'Articulos', 'id_usu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'fname' => 'Fname',
			'lname' => 'Lname',
			'direccion' => 'Direccion',
			'pass' => 'Pass',
			'date' => 'Date',
			'expdate' => 'Expdate',
			'status' => 'Status',
			'user_type' => 'User Type',
			'usuario' => 'Usuario',
			'telefono' => 'Telefono',
			'documento' => 'Documento',
			'id_ciu' => 'Id Ciu',
			'balance' => 'Balance',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('expdate',$this->expdate,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('documento',$this->documento);
		$criteria->compare('id_ciu',$this->id_ciu);
		$criteria->compare('balance',$this->balance);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
