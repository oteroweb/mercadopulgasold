<?php 
class ValidarRegistro Extends CFormModel {
	public $nombre;
public $email;
public $password;
public $repetir;
public $termino;
public $captcha;
public $verifyCode;
	public function rules()
	{
		return array
		(
			array('nombre','required','message'=>'Este campo'),
			array
			(
				'nombre',
				'match',
				'pattern'=>'/^[a-zA-Z\s]+$/',
				'message'=>'Este match error',
				
			),
			array
			(
				'nombre',
				'length',
				'min'=>5,
				'tooShort'=>'minimo',
				'max'=>50,
				'tooLong'=>'max',
				
			),
			/*validacion del Email*/
			array 
			(
				'email',
				'required',
				'message'=>'campo mailrequerido'
			),
			array
			(
			'email',
			'email',
				'message'=>'Este email erroreno error',
				),
			array(
			'email',
			'length',
			'min'=>8,
			'tooShort'=>'mal',
			'min'=>30,
			'tooLong'=>'malo ',
				),
			
			array('password','required','message'=>'Este campo'),
			array('password','match','pattern'=>'/^([a-z]+[0-9]+)|([0-9]+[a-z]+)/i','message'=>'Este match error',),
			array(
			'password',
			'length',
			'min'=>8,
			'tooShort'=>'mal',
			'min'=>30,
			'tooLong'=>'malo '
				),
			array(
					'repetir','compare','compareAttribute'=>'password','message'=>'no son iguales',
				),
			array(
				'termino',
				'required',
				'message'=>'es requerido',
				),
			 array('verifyCode','captcha',
        'allowEmpty'=>!extension_loaded('gd'),'on'=>'insert'
			  // 'showRefreshButton'=>false,
              // 'clickableImage'=>true
              )


			);	
	}
}

 ?>