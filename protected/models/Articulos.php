<?php

/**
 * This is the model class for table "articulos".
 *
 * The followings are the available columns in table 'articulos':
 * @property integer $id
 * @property string $articulo
 * @property string $subtitulo
 * @property string $foto1
 * @property string $foto2
 * @property string $estado
 * @property integer $ventas
 * @property integer $stock
 * @property integer $precio
 * @property string $tipopago
 * @property integer $id_usu
 * @property string $descripcion
 * @property string $garantia
 * @property string $tipopublicacion
 * @property string $fechaexp
 * @property string $fechainc
 * @property integer $id_cat
 *
 * The followings are the available model relations:
 * @property Categorias $idCat
 * @property Usuarios $idUsu
 */
class Articulos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articulos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('articulo, subtitulo, estado, stock, precio, tipopago, garantia, id_cat', 'required'),
			array('ventas, stock, precio, id_usu, id_cat', 'numerical', 'integerOnly'=>true),
			array('articulo, tipopago', 'length', 'max'=>255),
			array('estado', 'length', 'max'=>5),
			array('garantia', 'length', 'max'=>30),
			array('tipopublicacion', 'length', 'max'=>9),
			// The following rule is used by search().
			array('descripcion','filter', 'filter'=>'strip_tags'),
			//array('foto1, foto2', 'file', 'types'=>'jpg, png', 'safe' => false, 'allowEmpty'=>true, 'message'=> 'imagen invalida'),


				//array('foto1, foto2', 'file', 'types'=>'jpg, png', 'safe' => false, 'allowEmpty'=>true, 'on'=>'update', 'on'=>'insert'),
			  //array('foto1, foto2', 'file', 'allowEmpty'=>true, 'types'=>'jpg, jpeg', 'message'=>'Você só pode enviar imagens do tipo "JPG" no campo {attribute}.'),
			// @todo Please remove those attributes that should not be searched.
			array('id, articulo, subtitulo,  estado, ventas, stock, precio, tipopago, id_usu, descripcion, garantia, tipopublicacion, fechaexp, fechainc, id_cat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCat' => array(self::BELONGS_TO, 'Categorias', 'id_cat'),
			'idUsu' => array(self::BELONGS_TO, 'Usuarios', 'id_usu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id Art',
			'articulo' => 'Articulo',
			'subtitulo' => 'Subtitulo',
			'foto1' => 'Foto1',
			'foto2' => 'Foto2',
			'estado' => 'Estado',
			'ventas' => 'Ventas',
			'stock' => 'Cantidad para la venta',
			'precio' => 'Precio',
			'tipopago' => 'Tipopago',
			'id_usu' => 'Id Usu',
			'descripcion' => 'Descripcion',
			'garantia' => 'Garantia',
			'tipopublicacion' => 'Tipopublicacion',
			'fechaexp' => 'Fechaexp',
			'fechainc' => 'Fechainc',
			'id_cat' => 'Id Cat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

	public function beforeSave(){
   if(parent::beforeSave()){
   { 
         // for example
        $this->descripcion = CHtml::encode($_POST['Articulos']['descripcion']); // if you save dates as INT
        $this->id_usu = Yii::app()->user->getId();
        //if($this->isNewRecord){
        $this->ventas = 0;
    	//}
          $this->tipopublicacion = "basico";
            $this->fechaexp = date("Y");
              $this->fechainc = date("Y");;
        
        return true;
   }
   return false;
} }
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('articulo',$this->articulo,true);
		$criteria->compare('subtitulo',$this->subtitulo,true);
		$criteria->compare('foto1',$this->foto1,true);
		$criteria->compare('foto2',$this->foto2,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('des',$this->ventas);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('tipopago',$this->tipopago,true);
		$criteria->compare('id_usu',$this->id_usu);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('garantia',$this->garantia,true);
		$criteria->compare('tipopublicacion',$this->tipopublicacion,true);
		$criteria->compare('fechaexp',$this->fechaexp,true);
		$criteria->compare('fechainc',$this->fechainc,true);
		$criteria->compare('id_cat',$this->id_cat);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Articulos the static model class
	 */


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
